import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import VueAwesomeSwiper from 'vue-awesome-swiper'

import 'swiper/dist/css/swiper.css'

Vue.use(VueAwesomeSwiper, /* { default global options } */)

/* import VConsole from 'vconsole/dist/vconsole.min.js'
new VConsole() */

// import 'lib-flexible'
import '../element-variables.scss'
import './assets/css/basic.scss'

/* 国际化 */
import VueI18n from 'vue-i18n';
import zhLocale from './i18n/zh';
import enLocale from './i18n/en';
import { default as enL } from 'element-ui/lib/locale/lang/en'
import { default as zhL } from 'element-ui/lib/locale/lang/zh-CN'
import locale from 'element-ui/lib/locale'

router.beforeEach(async (to, from, next) => {
  next()
})

Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: 'en',
  messages: {
    zh: {
      ...zhLocale,
      ...zhL.el
    },
    en: {
      ...enLocale,
      ...enL.el
    }
  }
})

// Vue.use(ElementUI);
Vue.use(ElementUI, {
    locale: enL
  }
)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
