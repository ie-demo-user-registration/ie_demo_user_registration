const getters = {
	scrollHeight: state => state.home.scrollHeight,
	isDownUp: state => state.home.isDownUp
};
export default getters
