import device from 'current-device'

const user = {
  state: {
    scrollHeight: 0,
    isDownUp: false
  },
  mutations: {
    SET_SCROLL_HEIGHT: (state,val) => {
      state.scrollHeight = val;
    },
    SET_IS_DOWN_UP: (state,val) => {
      state.isDownUp = val;
    }
  },
  actions: {
    async test({ commit, state },params) {
      
    }
  },
  getters: {
    
  }
};

export default user;