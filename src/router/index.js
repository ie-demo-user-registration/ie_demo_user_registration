import Vue from 'vue'
import Router from 'vue-router'
import login from '@/views/login'
import home from '@/views/home'
import layout from '@/views/layout'
import undefinedPage from '@/views/error/404';
import userRouter from "@/router/user"

Vue.use(Router);

export const routes = [
	{ 
		path: '/',
		redirect: { 
			name: "ibAccount"
		}
	},
	{ 
		path: '/login', 
		name: 'login', 
		component: login 
	},
	// {
	// 	path: '/404',
	// 	name: '404',
	// 	component: undefinedPage,
	// },
	// { 
	// 	path: '/home', 
	// 	name: 'home', 
	// 	component: layout 
	// },
	// {
	// 	path: '*',
	// 	redirect: '/404'
	// },
	...userRouter
	
];

export default new Router({
	// mode:'history',
	routes
});

