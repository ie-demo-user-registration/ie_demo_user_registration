import layout from '@/views/layout'
import ibAccount from '@/views/user/ibAccount'
let userRouter = [
	{
		path: '/user',
		name: 'user',
		component: layout,
		children: [
			{
				path: 'ibAccount',
				name: 'ibAccount',
				component: ibAccount
      },
      
		]
	}
];
export default userRouter;