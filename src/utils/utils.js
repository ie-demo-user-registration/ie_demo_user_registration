/* 获取当前日期 y-m-d */
export function getNowYear(){
    let tDate = new Date();
    let str = tDate.getFullYear();
    return str;
}

/* 获取18年前的今天 */
export function getNowDate(){
    var date    = new Date();  
    var sign1   = "-";  
    var sign2   = ":";  
    var year    = date.getFullYear() // 年  
    var month   = date.getMonth() + 1; // 月  
    var day     = date.getDate(); // 日

    // 给一位数数据前面加 “0”  
    if (month >= 1 && month <= 9) {  
        month = "0" + month;  
    }  
    if (day >= 0 && day <= 9) {  
        day = "0" + day;  
    }
    var currentdate = (year - 18) + sign1 + month + sign1 + day;
    return currentdate
}

/* （Y-M-D h:i:s）格式转时间戳
*   @dateValue （Y-M-D h:i:s）格式的时间
*   @return 时间戳以（秒/S）为单位
*/
export function getTimestamp(dateValue){
    let timestamp = Date.parse(new Date(dateValue)) / 1000;
    return timestamp; 
}

/* 获取地址栏get传值的方法  */ 
export function getQueryString(){
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");  
    var r = window.location.search.substr(1).match(reg);  
    if (r != null) return decodeURI(r[2]);  
    return null; 
}

/* 首字母大写 */
export function strToUpper(str){
    let tData = str.substring(0,1).toUpperCase() + str.substring(1);
    return tData; 
}
