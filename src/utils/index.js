const CryptoJS = require('crypto-js');
const sha256 = require('crypto-js/sha256');

const KEY = 'ag4I1zcM$4@c9FQZ2O**12xaq1kk1mm1'
const IV = sha256('NADClinicOpenApi').toString().substring(0, 16)

export function encrypt(message) {
  let encrypt = CryptoJS.AES.encrypt(JSON.stringify(message), CryptoJS.enc.Utf8.parse(KEY), {
    iv: CryptoJS.enc.Utf8.parse(IV),
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
    // format: CryptoJS.format.OpenSSL
  }).toString()
  return new Buffer(encrypt).toString('base64');
}

export function decrypt(message) {
  let decrypt = CryptoJS.AES.decrypt(decodeURIComponent(new Buffer(message, 'base64').toString().replace(/\s/g, '')), CryptoJS.enc.Utf8.parse(KEY), {
    iv: CryptoJS.enc.Utf8.parse(IV),
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
    // format: CryptoJS.format.OpenSSL
  })
  return JSON.parse(decrypt.toString(CryptoJS.enc.Utf8));
}