import axios from 'axios'
import store from 'store/index.js';
import router from 'router'
import qs from 'qs'
// 创建axios实例
const service = axios.create({
    baseURL: process.env.VUE_APP_API, // api的base_url
    timeout: 150000 // 请求超时时间
});
// request拦截器
service.interceptors.request.use( config => {
    config.headers['access-token'] = "7403f0291437c47e589fdd0445860f95";
    config.data = qs.stringify(config.data)
    return config
},
error => {
    console.log(error);
    Promise.reject(error);
})

//respone拦截器
service.interceptors.response.use(
    response => response,
    error => {
        console.log('responseError=>' + error);
    }
)

export function axiosPost(url, params) {
    return new Promise((resolve, reject) => {
    service.post(url, params)
        .then(response => {
            resolve(response.data);
        }, err => {
            reject(err);
        })
        .catch((error) => {
            resolve({code:99999,msg:'网络出错'})
        })
    })
}

export function axiosGet(url, params) {
    return new Promise((resolve, reject) => {
    service.get(url, { params:params})
        .then(response => {
            resolve(response.data);
        }, err => {
            reject(err);
        })
        .catch((error) => {
            resolve({code:99999,msg:'网络出错'})
        })
    })
}

// 创建axios实例 上传图片
const imgService = axios.create({
    baseURL: process.env.VUE_APP_IMG_API, 
    timeout: 150000, // 请求超时时间
});

let imgConfig = {
    headers: { 'Content-Type': 'multipart/form-data' },
}
export function imgAxios(url, params) {
    return new Promise((resolve, reject) => {
        imgService.post(url, params, imgConfig)
        .then(response => {
            resolve(response.data);
        }, err => {
            reject(err);
        })
        .catch((error) => {
            resolve({code:99999,msg:'网络出错'})
        })
    })
}
