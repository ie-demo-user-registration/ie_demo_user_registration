import { axiosGet, axiosPost , imgAxios } from '@/api/axios'
import Qs from 'qs'

/**
 * requestDemo
 * @param ServiceRequest $request
 * param info
 */
export function requestDemo(query) {
    return axiosPost('/api/requestDemo', query);
}

/* 刷新验证码 */
export function refreshCode(query) {
    return axiosGet('/captcha',query);
}

/* 发送手机验证码 */
export function verifymobilecodesend(query) {
    return axiosPost('/api/sendMobileCode',query);
}

/* 验证手机验证码 */
export function verificationmobile(query) {
    return axiosPost('/api/checkMobileCode',query);
}

/* 注册真实账户 第一步 */
export function liveFirst(query) {
    return axiosPost('/api/liveFirst',query);
}

/* 注册真实账户 第二步 */
export function liveSecond(query) {
    return axiosPost('/api/liveSecond',query);
}

/* 国家列表 */
export function getCountries(query) {
    return axiosPost('/api/getCountries', Qs.stringify(query));
}

/* 上传图片 */
export function uploadImages(query) {
    return imgAxios('/login/upload_kyc_from_site', query);
}

/* 验证手机/邮箱 */
export function checkMobileEmail(query) {
    return axiosPost('/api/checkMobileEmail', query);
}

/* 注册模拟账户 */
export function demoRegister(query) {
    return axiosPost('/api/demo', Qs.stringify(query));
}

/* IB注册 第一步 */
export function requestIbFirst(query) {
    return axiosPost('/api/ib_register/first_step', query);
}

/* IB注册 第二步 */
export function requestIbSecond(query) {
    return axiosPost('/api/ib_register/second_step', query);
}

/* IB注册 第三步 */
export function requestIbThree(query) {
    return axiosPost('/api/ib_register/last_step', query);
}

/* 获取plan信息 */
export function getAllPlans(query) {
    return axiosPost('/api/getAllPlans', query);
}




















