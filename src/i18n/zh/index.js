import base from './base';
import menu from './menu';
import form from './form';
import register from './register';

export default {
  base,
  menu,
  form,
  register
};
