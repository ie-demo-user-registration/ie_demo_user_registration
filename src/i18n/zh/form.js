
let form = {
    firstname: '名字',
    lastname: '姓氏',
    email: '邮箱',
    mobile: '电话',
    phone: '手机',
    matters: '反馈事项',
    theme: '主题',
    content: '内容',
    title: '称谓',
    birthday: '生日',
    address: '地址',
    city: '城市',
    province: '省份',
    postCode: '邮编',
    confirmEmail: '确认邮箱',
    country: '国家',
    liveTime: '居住时间',
    currencyType: '货币类型',
    investment: '投资额度',
    identityProve: '身份证明',
    addressProve: '地址证明',
    materes: '合同材料',
    selectTitle: '请选择称谓',
    selectDate: '请选择日期',
    enterAddress: '请输入地址（请与地址证明保持一致）',
    enterCity: '请输入城市',
    enterProvince: '请输入省份',
    enterPostcode: '请输入邮编',
    selectLivetime: '请选择居住时间（年）',
    selectCurrency: '请选择货币类型',
    initialInvest: '您预计的最初投资额度',
    submit: '提 交',
    isRequired: '此项必填',
    enterCorrectEmail: '请输入正确的邮箱地址',
    enterCorrectMobile: '请输入正确的手机号码',
    submitSuccess: '提交成功',
    accNumber: '账户号',
    validVerify: '输入有效验证码',
    submitApply: '提交申请',
    regSuccess: '注册成功',
    againEmailError: '两次邮箱不一样',
    mobileError: '手机号码不正确',
    lessTwopoint: '请输入数字，最多两位小数',
    fileReqiured: '文件必须上传',
    verifyMobile: '请验证手机号码',
    sendCodeSuccess: '发送验证码成功',
    mobileVerifySuccess: '手机验证成功',
    mobileVerifyError: '手机验证失败',
    uploadError: '上传图片失败',
    enterRequired: '请填写必填项',
    formatError: '格式不正确',
    mr: '先生',
    mrs: '夫人',
    ms: '女士',
    dr: '博士',
    pleaseSelect: '请选择',
    enterFirstname: '名：请以拼音格式填写',
    enterLastname: '姓：请以拼音格式填写',
    enterMobile: '请输入手机号码',
    enterEmail: '请输入邮箱',
    enterEmailAgain: '请重新输入邮箱',
    selectCountry: '请选择国家',
    enterCountry: '请输入国家',
    rightCode: '请输入有效验证码',
    register: '注 册',
    clickVerify: '点击验证',
    nextStep: '继续下一步',
    verify: '验 证',
    note: '注：',
    isRequiredInfo: '为必填信息',
    enterInt: '请输入正整数',
    verifySuccess: '验证成功',
    verifyEmailFirst: '请先验证邮箱',
    have: '有',
    haveNot: '没有',
    yes: '是',
    no: '否',
    next: '下一步',
    last: '上一步',
    consult: '咨询内容',
    ac: '开户咨询',
    ad: '出入金问题',
    ae: 'IB业务',
    af: '客服回电',
    ag: '请选择咨询内容',
    ah: '在线咨询',
    aj: 'DEMO客户管理后台登录',
    ak: '客户管理后台',
    al: '立即登录',
    am: '恭喜您获得7天免费试用',
    an: '立即领取',
    ao: '立即下载',
    ap: '现货',
    aq: '手',
    ar: '小时',
    as: '请先选择账户类型',
    at: '请先填写基本信息',
    get_code: '免费获取验证码',
    send_code: '重发验证码',
    enter_code: '输入验证码',
    confirm: '确 认',
    phone_auth: '手机号码认证',
    leak_it: '校验码已发送至您的手机，5分钟内输入有效，请勿泄露！',
    most_four: '最多上传4张',
    most_ten: '最多上传10张',
    less_five: '上传图片大小不能超过 500kb!'
}

export default form;

































