const base = {
  b_required: '此项必填',
  b_invalid: '格式错误',
  b_select_date: '请选择日期',
  b_cancel: '取 消',
  b_submit: '确 定',
}
export default base;