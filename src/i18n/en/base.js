const base = {
  b_required: 'The field is required',
  b_invalid: 'Invalid format',
  b_select_date: 'Select Date',
  b_cancel: 'Cancel',
  b_submit: 'OK',
}
export default base;