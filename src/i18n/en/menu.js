const menu = {
  home: 'Home',
  products: 'Products',
  crm: 'CRM',
  clientPortal: 'Trader/Client Portal',
  ibPortal: 'IB Portal',
  ieAl: 'IE AI',
  ieMarketing: 'IE Marketing',
  service: 'Service',
  mtWhiteLabel: 'MT4/MT5  White Label',
  paymentSolution: 'Payment Solution',
  overseasSupport: 'Overseas Support',
  complianceConsultation: 'Compliance Consultation',
  licenseApplication: 'License Application',
  company: 'Company',
  aboutUs: 'About Us',
  careers: 'Careers',
  contactUs: 'Contact us'
}
export default menu;