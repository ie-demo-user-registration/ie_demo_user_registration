let form = {
  firstname:'First Name',
  lastname:'Last Name',
  email:'eMail',
  mobile:'Phone',
  phone:'Mobile phone',
  matters:'Contact Me About',
  theme:'Subject',
  content:'Messages',
  title:'Title',
  birthday:'date of birth',
  address:'Address',
  city:'City',
  province:'Province',
  postCode:'Postcode',
  confirmEmail:'Confirm Email',
  country:'Country',
  liveTime:'Number of years at this address',
  currencyType:'Currency Type',
  investment:'Investment quota',
  identityProve:'Proof of identity',
  addressProve:'Proof of address',
  materes:'Contract material',
  selectTitle:'Select title',
  selectDate:'Select date of birth',
  enterAddress:'Enter address(Please be consistent with the proof of address)',
  enterCity:'Enter city',
  enterProvince:'Enter province',
  enterPostcode:'Enter postcode',
  selectLivetime:'Select number of years at this address',
  selectCurrency:'Select currency type',
  initialInvest:'Initial deposit',
  submit:'Submit',
  isRequired:'This field is required',
  enterCorrectEmail:'Enter correct email',
  enterCorrectMobile:'Enter correct mobile phone',
  submitSuccess:'Submit success',
  accNumber:'Account Number',
  validVerify:'Enter a valid verification code',
  submitApply:'Submit an application',
  regSuccess:'Register success',
  againEmailError:'Email error',
  mobileError:'Mobile error',
  lessTwopoint:'Please enter a number, up to two decimal places',
  fileReqiured:'File must be uploaded',
  verifyMobile:'Please verify your mobile number',
  sendCodeSuccess:'Send verification code successfully',
  mobileVerifySuccess:'Mobile verified success',
  mobileVerifyError:'Mobile verified error',
  uploadError:'Upload error',
  enterRequired:'Fill in the required fields',
  formatError:'Format error',
  mr:'Mr.',
  mrs:'Mrs.',
  ms:'Ms.',
  dr:'Dr.',
  pleaseSelect:'Select',
  enterFirstname:'First Name:',
  enterLastname:'Last Name:',
  enterMobile:'Enter mobile phone',
  enterEmail:'Enter email',
  enterEmailAgain:'Re-enter email',
  selectCountry:'Select country',
  enterCountry:'Enter country',
  rightCode:'Enter a valid verification code',
  register:'Register',
  clickVerify:'Click Verification',
  nextStep:'Next Step',
  verify:'Verify',
  note:'Note',
  isRequiredInfo:'Required Information',
  enterInt:'Please enter a positive integer.',
  verifySuccess:'verify success',
  verifyEmailFirst:'Verify the email first',
  have:'Yes',
  haveNot:'No',
  yes:'Yes',
  no:'No',
  next:'Next',
  last:'Previous',
  consult:'Consulting Content',
  ac:'open account consulting',
  ad:'Access money problem',
  ae:'IB business',
  af:'Customer Service Call',
  ag:'please select consultation content',
  ah:'Consulting Online',
  aj:'DEMO Client Portal login',
  ak:'Client Portal',
  al:'Login now',
  am:'Congratulations, You Got 7 Days for Trial',
  an:'Get Now',
  ao:'Download',
  ap:'Spot',
  aq:'lots',
  ar:'hours',
  as:'Please select account type first',
  at:'Please fill in the basic information first',
  get_code:'Send Code',
  send_code:'Send Code',
  enter_code:'Enter code',
  confirm:'Confirm',
  phone_auth:'Phone Authentication',
  leak_it:'The verification code has been sent to your phone. It is valid for input within 5 minutes. <br />Please do not leak it!',
  most_four:'Upload 4 copies at most',
  most_ten:'Upload 10 copies at most',
  less_five:'Upload image size cannot exceed 500kb!',
}

export default form;